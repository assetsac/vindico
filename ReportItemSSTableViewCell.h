//
//  ReportItemSSTableViewCell.h
//  Vindico
//
//  Created by Belinda Natividad on 6/16/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportItemSSTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblReportItemSSHeader;
@property (weak, nonatomic) IBOutlet UITextView *txtReportItemSSAve;
@property (weak, nonatomic) IBOutlet UITextView *txtGroupHeader;
@property (weak, nonatomic) IBOutlet UITextView *txtSubGroupHeader;
@property (weak, nonatomic) IBOutlet UITextView *txtAverage;
@property (weak, nonatomic) IBOutlet UILabel *lblSubHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblRSubHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblRSubHeaderVal;

@end
