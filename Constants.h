//
//  Constants.h
//  Vindico
//
//  Created by Belinda Natividad on 8/23/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

#pragma mark - API URL KEYS
extern NSString *const kBaseUrl;

//// POST
//extern NSString *const kPostRegisterUrl;
//extern NSString *const kPostLoginUrl;
//extern NSString *const kPostResetUrl;
//extern NSString *const kPostGroupListUrl;
//extern NSString *const kPostListSavedReportUrl;
//extern NSString *const kPostSaveReportUrl;
//extern NSString *const kPostListSavedSearchUrl;
//extern NSString *const kPostBrandsLocationsUrl;
//extern NSString *const kPostSaveSearchUrl;
//
//// GET
//extern NSString *const kGetGameHistory;

@end
