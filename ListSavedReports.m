//
//  ListSavedReports.m
//  Vindico
//
//  Created by Belinda Natividad on 6/15/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "ListSavedReports.h"
//#import "ReportListSavedSearch.h"
#import "ListSavedSearch.h"

@implementation ListSavedReports

- (instancetype)initWithData:(NSString*) reportItemId reportName:(NSString*) reportItemName createdDate:(NSString*) reportDate listReports:(NSArray*) reportListItems {
    
    if(self = [super init]) {
        
        _reportId               = reportItemId;
        _reportName             = reportItemName;
        _reportCreatedDate      = reportDate;
        [self parseReportsData: reportListItems];
        //_reports                = reportListItems;
        
        
        //NSLog(@"MODEL %@ %@ %@ %@",_reportId,_reportName,_reportCreatedDate);
        
        
        

    }
    return self;

    
}

- (void)parseReportsData:(NSArray*) groups{
    _reportsArray = [NSMutableArray array];
    for (NSDictionary *data in groups){
        ListSavedSearch *itemReport = [[ListSavedSearch alloc]initWithData:data[@"location_id"] locationName:data[@"location_name"] brandId:data[@"brand_id"] brandName:data[@"brand_name"] areaRange:data[@"area_range"] searchGroups:data[@"search_group"] listGroups:data[@"groups"]];
        [_reportsArray addObject:itemReport];
    }
}

@end
