 //
//  SearchViewController.m
//  Vindico
//
//  Created by Belinda Natividad on 5/10/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "SearchViewController.h"
#import "Item.h"
#import "ItemSubgroup.h"
#import "Brand.h"
#import "GroupTableViewController.h"
#import "RequestManager.h"
#import "BrandList.h"
#import "LocationList.h"

@interface SearchViewController () <UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource> {
    NSDictionary *brands;
    NSDictionary *locations;
    NSDictionary *brand;
    NSDictionary *location;
    
    NSMutableArray *brandList;
    NSMutableArray *brandIdList;
    NSMutableArray *locationList;
    id responseObj;
    NSMutableDictionary *brandMaster;
    
    NSArray *tblContent;
    //NSMutableArray *headerData;
    
    NSDictionary *searchItems;
    NSArray *sampleCItem;
    NSDictionary *itemC;
    NSMutableArray *itemList;
   // NSArray *sampleCItem;
    
    NSMutableArray *filterBrandArray;
    NSMutableArray *filterLocationArray;
    NSArray *filterAreaRangeArray;
    NSString *selectedBrand;
    NSString *selectedLocation;
    NSString *selectedAreaRange;
    
    UIButton *button;
    
    AFHTTPRequestOperationManager *manager;
    NSString *getUrl;
    NSString *locationID;
    NSString *brandID;
    NSString *areaID;
    
    NSString *selectedItem;
    
    NSMutableArray *savedSearch;
    NSMutableDictionary *savedSearchItem;
    NSString *savedAve;
    
    NSMutableArray *tmpSaveSearchList;
    
    NSMutableArray *savedSearchtoAPI;
    NSMutableDictionary *savedSearchItemtoAPI;
    
    UIAlertController *alertWarning;
    NSString *alertWarningTitle;
    NSString *alertWarningMsg;
    
    NSInteger *subGroupCount;
    
    int xBrandId;
    int xLocationId;
    

}


@property (strong, nonatomic) NSArray *headerTable;

@property (nonatomic) NSInteger selectedGroupId1;
@property (nonatomic) NSInteger selectedGroupId2;
@property (nonatomic) NSInteger selectedGroupId3;

@end



@implementation SearchViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    filterBrandArray = [[NSMutableArray alloc]init];
    filterLocationArray = [[NSMutableArray alloc]init];
    
    
    filterAreaRangeArray = @[@"1-500",@"501-1000",@"1001-1500",@"1501-2000"];
    
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    
    self.tblSResults.dataSource = self;
    self.tblSResults.delegate = self;
    //[self.tblSResults reloadData];
    
    savedSearch = [[NSMutableArray alloc]init];
    
    self.selectedGroupId1 = 1;
    self.selectedGroupId2 = 2;
    self.selectedGroupId3 = 3;
    
    [self.btnTextGroupName setTitle:@"Electrical" forState:UIControlStateNormal];
    [self.btnTextGroupName2 setTitle:@"Gas" forState:UIControlStateNormal];
    [self.btnTextGroupName3 setTitle:@"Air Conditioning" forState:UIControlStateNormal];
    
    self.subGroupLabel1.text = @"";
    self.subGroupLabel2.text = @"";
    self.subGroupLabel3.text = @"";
    [self.txtViewAve setHidden:YES];
    [self.viewAverage setHidden:YES];
    
    self.navigationItem.hidesBackButton = YES;
    
    [self.lblResultCount setHidden:YES];
    self.lblResultCount.text = @"";
  
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_viewFilter setHidden:YES];
    [self.viewAverage setHidden:YES];
    _lblSelectedBrand.text =  @"";
    _lblSelectedLocation.text = @"";
    _lblSelectedArea.text = @"";
    brandID =  nil;
    locationID = nil;
    [_tblSResults reloadData];
    [_tblSResults setHidden:YES];
    _txtViewAve.text = @"";
    [_pickerView reloadAllComponents];
    [_pickerView selectRow:0 inComponent:0 animated:YES];
    [_pickerView selectRow:0 inComponent:1 animated:YES];
    [_pickerView selectRow:0 inComponent:2 animated:YES];
    self.subGroupLabel1.text = @"";
    self.subGroupLabel2.text = @"";
    self.subGroupLabel3.text = @"";
    [self.lblResultCount setHidden:YES];
    self.lblResultCount.text = @"";


//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewReload) name:@"viewReload" object:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - Button Events

-(IBAction)btnFilterSearch:(id)sender {
    
    [self getPickerData];
    
    _pickerView.hidden = NO;
    [self.view addSubview:_pickerView];
    
    float btnXCoord = self.pickerView.bounds.size.width - 125;
    float btnYCoord = self.pickerView.frame.origin.y + 10;
    NSLog(@"%f",btnYCoord);
    
    
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(btnDoneClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Search" forState:UIControlStateNormal];
    
//    button.frame = CGRectMake(900.0, 550.0, 70.0, 40.0);
    button.frame = CGRectMake(btnXCoord, btnYCoord, 70.0, 40.0);
    
    
    [self.view addSubview:button];
    
    
}

-(void)btnDoneClicked:(UIButton *)sender {
    
    _viewFilter.hidden = NO;
    _pickerView.hidden = YES;
    button.hidden = YES;
    [_tblSResults setHidden:NO];
    [self.txtViewAve setHidden:NO];
    [self.viewAverage setHidden:NO];
    self.aveFirstCol.text = @"";
    self.aveSecCol.text = @"";
    self.aveThirdCol.text = @"";
    
    [_pickerView resignFirstResponder];

    selectedBrand = ((BrandList *)filterBrandArray[[self.pickerView selectedRowInComponent:0]]).brandSearchName;
    selectedLocation = ((LocationList *)filterLocationArray[[self.pickerView selectedRowInComponent:1]]).locationSearchName;
    selectedAreaRange = filterAreaRangeArray[[self.pickerView selectedRowInComponent:2]];
    
    _lblSelectedBrand.text =  selectedBrand;
    _lblSelectedLocation.text = selectedLocation;
    _lblSelectedArea.text = selectedAreaRange;
    
    
    brandID = ((BrandList *)filterBrandArray[[self.pickerView selectedRowInComponent:0]]).brandSearchID;
    locationID = ((LocationList *)filterLocationArray[[self.pickerView selectedRowInComponent:1]]).locationSearchID;

    
    [self checkEmptyGroups];
    
//    self.lblResultCount.text = [NSString stringWithFormat:@"%ld",subGroupCount];
    
    
    
    
}


- (IBAction)btnSaveReport:(id)sender {
    
    if (brandID == nil || locationID == nil || areaID == nil || self.selectedGroupId1 == 0 || self.selectedGroupId2 == 0 || self.selectedGroupId3 == 0 ) {
        
        alertWarningTitle   = @"Warning";
        alertWarningMsg     = @"Please select items to filter search";
        [self callWarningAlert];
        
    } else {
        
        savedSearchItem = [[NSMutableDictionary alloc] init];
        [savedSearchItem setObject:selectedBrand forKey:@"selectedBrand"];
        [savedSearchItem setObject:selectedLocation forKey:@"selectedLocation"];
        [savedSearchItem setObject:selectedAreaRange forKey:@"selectedAreaRange"];
        //[savedSearchItem setObject:savedAve forKey:@"savedAve"];
        [savedSearch addObject:savedSearchItem];
        
        NSLog(@"%@",savedSearch);
        
        [self saveSearchToAPI];
        
        
        UIAlertController *alertSave = [UIAlertController alertControllerWithTitle:@"Success" message:@"Results successfully saved to your report!" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
            [self performSegueWithIdentifier:@"toSaveReport" sender:self];
        }];
        
        [alertSave addAction:ok];
        [self presentViewController:alertSave animated:YES completion:nil];
        
        [self performSegueWithIdentifier:@"toSaveReport" sender:self];
    
    }
    
    
    

}

- (IBAction)btnLogout:(id)sender {

     UIAlertController *logoutAlertController = [UIAlertController alertControllerWithTitle:@"Logout" message:@"Are you sure you want to log out?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:NO completion:nil];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)  {
        [self dismissViewControllerAnimated:YES completion:nil];
        NSLog(@"Cancel action");
    }];
    
    [logoutAlertController addAction:cancelAction];
    [logoutAlertController addAction:yesAction];
    [self presentViewController:logoutAlertController animated:YES completion:nil];


    
}

- (IBAction)btnSRGroupName:(id)sender {
    
}


- (void) popoverDone:(id)sender {
//    _btnTextGroupName.text = [sender someValue];
//    [sender dismissPopoverAnimated:YES];
}


- (void)popOverItemSelected:(NSString *)selectedItem {
    selectedItem = _selectedCellText;
    [_btnTextGroupName setTitle:_selectedCellText forState:UIControlStateNormal];
    [_popOver dismissPopoverAnimated:YES];
    NSLog(@"%@",_selectedCellText);
}

-(void)didTap {
    //Dismiss your popover here;
    _popController.delegate = self;
    [_popController dismissPopoverAnimated:YES];
    
}


#pragma mark - API Requests

-(void) getBrandList {
    
    for (NSDictionary *brandI in [responseObj objectForKey:@"brands"]){
//      NSLog(@"Brand: %@ %@", [brand objectForKey:@"id"], [brand objectForKey:@"brand_name"]);
        BrandList *brandItem = [[BrandList alloc] initWithId:[brandI objectForKey:@"id"] brandName:[brandI objectForKey:@"brand_name"]];
        
        [filterBrandArray addObject:brandItem];
        
    }
    
}

-(void) getLocationList {
    
    for (NSDictionary *locationI in [responseObj objectForKey:@"locations"]) {
        
//      NSLog(@"Location: %@", [location objectForKey:@"location_name"]);
        LocationList *locationItem = [[LocationList alloc]initWithId:[locationI objectForKey:@"id"] locationName:[locationI objectForKey:@"location_name"]];
        [filterLocationArray addObject:locationItem];
    }
    
}

-(void) getPickerData {
    
    _accountToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    
    manager = [AFHTTPRequestOperationManager manager];
    [RequestManager callAPI];
    [manager.requestSerializer setValue:_accountToken forHTTPHeaderField:@"X-Auth-Token"];
    [manager GET:[NSString stringWithFormat:@"%@brands-and-location",kBaseUrl] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        responseObj = responseObject;
        
        [self getBrandList];
        [self getLocationList];
        
        [_pickerView reloadAllComponents];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}

-(void) getSearchResultsData {
    
    areaID = [selectedAreaRange stringByReplacingOccurrencesOfString:@"-" withString:@","];
    
    xBrandId    = [brandID intValue];
    xLocationId = [locationID intValue];

    
   getUrl = [NSString stringWithFormat:@"%@search-item?location_id=%d&brand_id=%d&area_range=[%@]&group_id=[%lu,%lu,%lu]",kBaseUrl,xLocationId,xBrandId,areaID,(long)self.selectedGroupId1,(long)self.selectedGroupId2,(long)self.selectedGroupId3];
    NSLog(@"%@",getUrl);
    NSLog(@"search result data brand id at location id %d %d %@",xBrandId,xLocationId,areaID);
    NSLog(@"search result data group ids %lu %lu %lu",(long)self.selectedGroupId1,(long)self.selectedGroupId2,(long)self.selectedGroupId3);
    //@"http://staging.straightarrowasset.com/vindico/public/api/v1/search-item?location_id=1&brand_id=8&area_range=[1,500]&group_id=[1,2,3]"

    _accountToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    
    manager = [AFHTTPRequestOperationManager manager];
    [RequestManager callAPI];
    [manager.requestSerializer setValue:_accountToken forHTTPHeaderField:@"X-Auth-Token"];

    [manager GET:getUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        searchItems = (NSDictionary *)responseObject;
        
        
        
        if(self.contentItems){
            [self.contentItems removeAllObjects];
        } else {
            self.contentItems = [NSMutableArray array];
        }
        
        for(NSDictionary *data in responseObject){
            Item *item =  [[Item alloc] initWithId:data[@"group_id"] groupName:data[@"group_name"] subgroups:data[@"sub_groups"]];
            [self.contentItems addObject:item];
        }
        
        [self.tblSResults reloadData];
        
        
        
        
            // display subgroup names
            NSMutableString *subGroup1MutableString = [[NSMutableString alloc] init];
            NSMutableString *subGroup2MutableString = [[NSMutableString alloc] init];
            NSMutableString *subGroup3MutableString = [[NSMutableString alloc] init];
            
            NSMutableString *subGroup1AveMutableString = [[NSMutableString alloc] init];
            NSMutableString *subGroup2AveMutableString = [[NSMutableString alloc] init];
            NSMutableString *subGroup3AveMutableString = [[NSMutableString alloc] init];
            
            // column 1
            Item *item = self.contentItems[0];
            NSInteger index1 = 1;
            for (ItemSubgroup *itemSubGroup in item.subgroupArray) {
                
                NSString *subGroupName  = itemSubGroup.subGroupName;
                NSString *subGroupAve   = itemSubGroup.average;
                [subGroup1AveMutableString appendString:[NSString stringWithFormat:@"(%ld) %@ | ",index1, subGroupAve]];
                [subGroup1MutableString appendString:[NSString stringWithFormat:@"(%ld) %@\n", index1,subGroupName]];
                index1++;
            }
            // change subgroup column 1 label
            self.subGroupLabel1.text    = subGroup1MutableString;
            self.aveFirstCol.text       = subGroup1AveMutableString;
            
            
            
            
            // column 2
            Item *item2 = self.contentItems[1];
            NSInteger index2 = 1;
            for (ItemSubgroup *itemSubGroup2 in item2.subgroupArray) {
                NSString *subGroupName2 = itemSubGroup2.subGroupName;
                NSString *subGroupAve2  = itemSubGroup2.average;
                [subGroup2AveMutableString appendString:[NSString stringWithFormat:@"(%ld) %@ | ",index2,subGroupAve2]];
                [subGroup2MutableString appendString:[NSString stringWithFormat:@"(%ld) %@\n",index2,subGroupName2]];
                index2++;
            }
            // change subgroup column 2 label
            self.subGroupLabel2.text    = subGroup2MutableString;
            self.aveSecCol.text         = subGroup2AveMutableString;
            
            // column 3
            Item *item3 = self.contentItems[2];
            NSInteger index3 = 1;
            for (ItemSubgroup *itemSubGroup3 in item3.subgroupArray) {
                NSString *subGroupName3 = itemSubGroup3.subGroupName;
                NSString *subGroupAve3   = itemSubGroup3.average;
                [subGroup3AveMutableString appendString:[NSString stringWithFormat:@"(%ld) %@ | ",index3, subGroupAve3]];
                [subGroup3MutableString appendString:[NSString stringWithFormat:@"(%ld) %@\n", index3, subGroupName3]];
                index3++;
            }
            // change subgroup column 3 label
            self.subGroupLabel3.text    = subGroup3MutableString;
            self.aveThirdCol.text       = subGroup3AveMutableString;

       
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    
    
}

-(void) saveSearchToAPI {
    
    _accountToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    
    
    manager = [AFHTTPRequestOperationManager manager];
    [RequestManager callAPI];
    [manager.requestSerializer setValue:_accountToken forHTTPHeaderField:@"X-Auth-Token"];

    //NSDictionary *parameters = @{@"location_id":locationID, @"brand_id":brandID, @"area_range":selectedAreaRange @"group_id":grouID};
    NSString *locIDtemp = [NSString stringWithFormat:@"%d",xLocationId];
    NSString *brandIDtemp = [NSString stringWithFormat:@"%d",xBrandId];
    NSString *areaIDtemp = [NSString stringWithFormat:@"[%@]",areaID];
    NSString *groupIds = [NSString stringWithFormat:@"[%ld,%ld,%ld]",self.selectedGroupId1,self.selectedGroupId2,self.selectedGroupId3];
     NSDictionary *parameters = @{@"location_id":locIDtemp, @"brand_id":brandIDtemp, @"area_range":areaIDtemp, @"group_id":groupIds};
 
    [manager POST:[NSString stringWithFormat:@"%@save-search",kBaseUrl] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"parameters!:%@",parameters);
        NSDictionary *responseDictionary = responseObject;
        
        if ([responseDictionary[@"message"] isEqualToString:@"Saved successfully"]) {
            //[self showSuccessAlert];
            NSLog(@"Search successfully saved to API");
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    


}

#pragma mark - Alert Controller 
-(void) callWarningAlert {
    
    alertWarning = [UIAlertController alertControllerWithTitle:alertWarningTitle message:alertWarningMsg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertWarning addAction:ok];
    [self presentViewController:alertWarning animated:YES completion:nil];

}


#pragma mark - Popover Delegate
-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    [self.popController dismissPopoverAnimated:YES];
}


#pragma mark - Table view data source


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    Item *item              = self.contentItems[0];
    ItemSubgroup *subgroup  = item.subgroupArray[0];
    NSLog(@"Heyyy %lu",(unsigned long)[subgroup.brandArray count]);
    
    
    UILabel *messageLbl = [[UILabel alloc] init];
   
    if(![subgroup.brandArray count] ) {
               //set the message
        messageLbl.text = @"No results found. Try another search";
        //center the text
        messageLbl.textAlignment = NSTextAlignmentCenter;
        //auto size the text
        [messageLbl sizeToFit];
        messageLbl.textColor = [UIColor whiteColor];
        
        //set back to label view
        _tblSResults.backgroundView = messageLbl;
        //no separator
        _tblSResults.separatorStyle = UITableViewCellSeparatorStyleNone;
        
      //  return 0;
    } else {
        messageLbl = [[UILabel alloc] init];
      //  messageLbl.b
        messageLbl.text = @"";
        [messageLbl setHidden:YES];
        _tblSResults.backgroundView = NO;
        _tblSResults.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    }
    
    if ([subgroup.brandArray count]== 0) {
         [self.lblResultCount setHidden:YES];

    } else {
        [self.lblResultCount setHidden:NO];
        self.lblResultCount.text = [NSString stringWithFormat:@"%lu result/s found",(unsigned long)[subgroup.brandArray count]] ;
    }
    return [subgroup.brandArray count];
    
//    return [_contentItems count];
    ///return 1;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SRContentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sResultCell" forIndexPath:indexPath];
    
    
    NSMutableString *valuesColumn1 = [[NSMutableString alloc] init];
    NSMutableString *valuesColumn2 = [[NSMutableString alloc] init];
    NSMutableString *valuesColumn3 = [[NSMutableString alloc] init];
    
    // column 1
    Item *item = self.contentItems[[indexPath section]];
    for (int i=0; i < [item.subgroupArray count]; i++) {
        int j =i + 1;
        ItemSubgroup *subs =item.subgroupArray[i];
        Brand *brandVal = subs.brandArray[indexPath.row];
        [valuesColumn1 appendString:[NSString stringWithFormat:@"(%i) %@ | ",j,brandVal.brandValue]];

    }
    cell.lblFirstCol.text = valuesColumn1;
    
    
    Item *item2 = self.contentItems[1];
    for (int i=0; i < [item2.subgroupArray count]; i++) {
        int k =i + 1;
        ItemSubgroup *subs =item2.subgroupArray[i];
        Brand *brandVal = subs.brandArray[indexPath.row];
        [valuesColumn2 appendString:[NSString stringWithFormat:@"(%i) %@ | ",k,brandVal.brandValue]];
        
    }
    cell.lblSecCol.text = valuesColumn2;
    
    Item *item3 = self.contentItems[2];
    for (int i=0; i < [item3.subgroupArray count]; i++) {
        int l =i + 1;
        ItemSubgroup *subs =item3.subgroupArray[i];
        Brand *brandVal = subs.brandArray[indexPath.row];
        [valuesColumn3 appendString:[NSString stringWithFormat:@"(%i) %@ | ",l,brandVal.brandValue]];
        
    }
    cell.lblThirdCol.text = valuesColumn3;

    return cell;
}


#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    switch (component) {
        case 0:
            return filterBrandArray.count;
        case 1:
            return filterLocationArray.count;
        default:
            return filterAreaRangeArray.count;
            
    }

}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    switch (component) {
        case 0:
//          return filterBrandArray[row];
//            int b = [[filterBrandArray objectAtIndex:row] intValue];
            return [NSString stringWithFormat:@"%@",((BrandList *)[filterBrandArray objectAtIndex:row]).brandSearchName];
        case 1:
            return [NSString stringWithFormat:@"%@", ((LocationList *)[filterLocationArray objectAtIndex:row]).locationSearchName];
        default:
            return filterAreaRangeArray[row];
    }
    
}

//- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
//    
//    NSString *strBrand;
//    NSString *strLocation;
//    NSString *strAreaRange;
//    
//    switch (component) {
//        case 0:
//            strBrand = [filterBrandArray objectAtIndex:row];
//            self.lblSelectedBrand.text = strBrand;
////            brandID = (long)((BrandList *)[filterBrandArray objectAtIndex:row]).brandSearchID;
//            break;
//            
//        case 1:
//            strLocation = [filterLocationArray objectAtIndex:row];
//            self.lblSelectedLocation.text = strLocation;
//            break;
//            
//        case 2:
//            strAreaRange = [filterAreaRangeArray objectAtIndex:row];
//            self.lblSelectedArea.text = strAreaRange;
//    }
//    
//}

#pragma mark - Navigation
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    UIPopoverPresentationController *popoverController = segue.destinationViewController.popoverPresentationController;
    popoverController.sourceRect = CGRectMake(0, 0, popoverController.sourceView.frame.size.width, (popoverController.sourceView.frame.size.height / 3) * 2);
    
    if ([segue.identifier isEqualToString:@"firstColSegue"]) {
        GroupTableViewController *destVC = segue.destinationViewController;
        destVC.selectedGroupBlock = ^(GroupItem *selectedFirstColString) {
            NSLog(@"First: %@", selectedFirstColString);
            
           self.selectedGroupId1 = selectedFirstColString.groupId;
            [self.btnTextGroupName setTitle:selectedFirstColString.groupName forState:UIControlStateNormal];
            [self getSearchResultsData];
            
            
        };
        
        destVC.selectedGroupIdA = self.selectedGroupId1;
        destVC.selectedGroupIdB = self.selectedGroupId2;
        destVC.selectedGroupIdC = self.selectedGroupId3;
    }
    

    if ([segue.identifier isEqualToString:@"secondColSegue"]) {
        GroupTableViewController *destVC2 = segue.destinationViewController;
        destVC2.selectedGroupBlock = ^(GroupItem *selectedSecondColString) {
            NSLog(@"Second: %@", selectedSecondColString);
            
            
            self.selectedGroupId2 = selectedSecondColString.groupId;
            [self.btnTextGroupName2 setTitle:selectedSecondColString.groupName forState:UIControlStateNormal];
            [self getSearchResultsData];
            
        };
        
        destVC2.selectedGroupIdB = self.selectedGroupId2;
        destVC2.selectedGroupIdA = self.selectedGroupId1;
        destVC2.selectedGroupIdC = self.selectedGroupId3;

    }
    
    if ([segue.identifier isEqualToString:@"thirdColSegue"]) {
        GroupTableViewController *destVC3 = segue.destinationViewController;
        destVC3.selectedGroupBlock = ^(GroupItem *selectedThirdColString) {
            NSLog(@"Third: %@", selectedThirdColString);
            
            
            self.selectedGroupId3 = selectedThirdColString.groupId;
            [self.btnTextGroupName3 setTitle:selectedThirdColString.groupName forState:UIControlStateNormal];
            [self getSearchResultsData];

        };
        
        destVC3.selectedGroupIdC = self.selectedGroupId3;
        destVC3.selectedGroupIdA = self.selectedGroupId1;
        destVC3.selectedGroupIdB = self.selectedGroupId2;

    }
    
    if ([segue.identifier isEqualToString:@"toSaveReport"]) {
        SavedSearchViewController *destVC = segue.destinationViewController;
        destVC.savedSearch = savedSearch;
        destVC.getUrl = getUrl;
        //((SavedSearchViewController *)[segue destinationViewController]).savedSearch = savedSearch;
              
    }
    
}

#pragma mark - Methods
-(void)clearColumnLabels {
    self.subGroupLabel1.text = @"";
    self.subGroupLabel2.text = @"";
    self.subGroupLabel3.text = @"";
    self.txtViewAve.text  = @"";
    [self.viewFilter setHidden:YES];
    [self.tblSResults setHidden:YES];
    [self.viewAverage setHidden:YES];
    
}


- (void) checkEmptyGroups {
    
    if ((long)self.selectedGroupId1 == (long)self.selectedGroupId2 || (long)self.selectedGroupId1 == (long)self.selectedGroupId3 || (long)self.selectedGroupId2 == (long)self.selectedGroupId3) {
        alertWarningTitle   = @"Warning";
        alertWarningMsg     = @"Column has been chosen";
        [self callWarningAlert];
        [self.tblSResults reloadData];
        
    } else {
        [self getSearchResultsData];
    }


}

@end
