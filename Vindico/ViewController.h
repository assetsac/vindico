//
//  ViewController.h
//  Vindico
//
//  Created by Belinda Natividad on 5/10/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ViewControllerDelegate <NSObject>

@end

@interface ViewController : UIViewController <UITextFieldDelegate>


- (IBAction)btnLogin:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

@property (nonatomic, assign) id<ViewControllerDelegate>delegate;
@property (strong, nonatomic) IBOutlet ViewController *logView;


@property(strong,nonatomic) UIAlertController *alertCtrl;

@end

