//
//  RegisterViewController.m
//  Vindico
//
//  Created by Belinda Natividad on 5/10/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "RegisterViewController.h"



@interface RegisterViewController ()

@end

NSString *atitle = @"Registration Failed!";
NSString *amsg = @"The username must be at least 6 characters.";
UIAlertController *alertCtrl;

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _txtUsername.delegate = self;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnRegister:(id)sender {

    
    if ([_txtFirstName.text isEqualToString:@""] || [_txtLastName.text isEqualToString:@""] || [_txtEmailAddress.text isEqualToString:@""] || [_txtUsername.text isEqualToString:@""] ||[_txtPassword.text isEqualToString:@""]) {
        amsg    = @"Fill up empty field/s";
        [self showFailAlert];
        
    } else if ([_txtUsername.text length] < 6 && [_txtUsername.text length] !=0) {
        amsg    = @"The username must be at least 6 characters.";
        [self showFailAlert];
        
    } else if ([_txtPassword.text length] < 6 && [_txtPassword.text length] !=0) {
        amsg    = @"The password must be at least 6 characters.";
        [self showFailAlert];
        
    } else if (![self validEmail:[NSString stringWithFormat:@"%@",_txtEmailAddress.text]]) {
        amsg    = @"Email format is not valid";
        [self showFailAlert];
        
    } else if (![_txtPassword.text isEqualToString:_txtRePassword.text]) {
        amsg    = @"Password didn't match";
        [self showFailAlert];
        
    } else {
       [self callAPI];
    NSLog(@"Success");
    }
   
}


- (void) showSuccessAlert {
    
    UIAlertController *alertCtrl = [UIAlertController alertControllerWithTitle:@"Registration" message:@"Registration successful!" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [self performSegueWithIdentifier:@"loginSeg" sender:self];
    }];
    
    [alertCtrl addAction:ok];
    [self presentViewController:alertCtrl animated:YES completion:nil];
}



- (void) showFailAlert {
    alertCtrl = [UIAlertController alertControllerWithTitle:atitle message:amsg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertCtrl addAction:cancel];
    [self presentViewController:alertCtrl animated:YES completion:nil];


}


- (BOOL) validEmail:(NSString*) emailString {
    
    if([emailString length]==0){
        return NO;
    }
    
    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    
   // NSLog(@"%i", regExMatches);
    if (regExMatches == 0) {
        return NO;
    } else {
        return YES;
    }
    
    
}

-(void) callAPI {

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSDictionary *parameters = @{@"first_name":_txtFirstName.text, @"last_name":_txtLastName.text, @"email":_txtEmailAddress.text, @"username":_txtUsername.text,@"password" :_txtPassword.text};
    [manager POST:[NSString stringWithFormat:@"%@auth/register",kBaseUrl] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"parameters!:%@",parameters);
        NSDictionary *responseDictionary = responseObject;
        
        if ([responseDictionary[@"message"] isEqualToString:@"Registered successfully"]) {
            [self showSuccessAlert];
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"JSON: %@", operation.responseObject);
        
        NSArray *temp = operation.responseObject;
        
        atitle  = [temp valueForKey:@"message"];
        amsg    = [temp valueForKey:@"error"];
        [self showFailAlert];
    }];

    
    

}


@end
