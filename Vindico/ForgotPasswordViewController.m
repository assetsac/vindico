//
//  ForgotPasswordViewController.m
//  Vindico
//
//  Created by Belinda Natividad on 5/10/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "ForgotPasswordViewController.h"

@interface ForgotPasswordViewController () {
    AFHTTPRequestOperationManager *manager;
    
    NSString *forgotUrl;
    NSString *resetUrl;
    
    NSString *successMsg;
    NSString *title;
    
    NSString *accountToken;
}

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnVerify:(id)sender {
    
    if ([_txtEmailAddress.text isEqualToString:@""] || ![self validEmail:[NSString stringWithFormat:@"%@",_txtEmailAddress.text]]) {
        [self showFailedAlert];
    } else  {
        [self forgotPassAPI];
        [self.txtEmailAddress setEnabled:NO];
    }
    
   
    
}

- (IBAction)btnChangePassword:(id)sender {
    
   if (([_txtResetPassword.text length] < 6 && [_txtResetPassword.text length] !=0) || ([_txtResetConfirmPassword.text length] < 6 && [_txtResetConfirmPassword.text length] !=0) ) {
        title         = @"Warning";
        successMsg    = @"The password must be at least 6 characters.";
        [self showSuccessAlert];
        
    } else if (![_txtResetPassword.text isEqualToString:_txtResetConfirmPassword.text]) {
        title         = @"Warning";
        successMsg    = @"Password didn't match";
        [self showSuccessAlert];
    } else {
        [self resetPassAPI];
    }

    
    
}

-(void) forgotPassAPI {
    
    accountToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    
    manager = [AFHTTPRequestOperationManager manager];
    [RequestManager callAPI];
    [manager.requestSerializer setValue:accountToken forHTTPHeaderField:@"X-Auth-Token"];

    // [manager.requestSerializer setValue:accountToken forHTTPHeaderField:@"X-Auth-Token"];
    
//    forgotUrl =@"http://www.vrdnet.net/api/public/api/v1/auth/forgot";
    
    NSDictionary *parameters = @{@"email":_txtEmailAddress.text};
    [manager POST:[NSString stringWithFormat:@"%@auth/forgot",kBaseUrl] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"parameters!:%@",parameters);
        
      //  BOOL statusTrue = YES;
        //NSLog(@"%@",statusTrue);
        
        NSNumber *isSuccessNumber = (NSNumber *)[responseObject objectForKey: @"status"];
        
        if ([isSuccessNumber boolValue] == YES) {
            title = @"Success";
            successMsg = [NSString stringWithFormat:@"Your password reset code has been sent to %@",_txtEmailAddress.text];
            [self showSuccessAlert];
            self.txtEmailAddress.backgroundColor = [UIColor lightGrayColor];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if (error !=nil) {
            [self showFailedAlert];
        }
    }];

}

- (void) resetPassAPI {
    
    manager = [AFHTTPRequestOperationManager manager];
    [RequestManager callAPI];
    // [manager.requestSerializer setValue:accountToken forHTTPHeaderField:@"X-Auth-Token"];
    
//    resetUrl =@"http://www.vrdnet.net/api/public/api/v1/auth/reset-password";
    
    
    NSDictionary *parameters = @{@"reset_code":_txtResetCode.text, @"new_password":_txtResetPassword.text, @"confirm_new_password":_txtResetConfirmPassword.text};
    [manager POST:[NSString stringWithFormat:@"%@auth/reset-password",kBaseUrl] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"parameters!:%@",parameters);
        
        if ([responseObject[@"message"] isEqualToString:@"Password change successfully"]) {
            title = @"Success";
            successMsg = @"Password changed successfully";
            NSLog(@"Password change succesful");
            
            
            UIAlertController *alertSuccess = [UIAlertController alertControllerWithTitle:title message:successMsg preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self dismissViewControllerAnimated:YES completion:nil];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
            
            [alertSuccess addAction:ok];
            [self presentViewController:alertSuccess animated:YES completion:nil];

            
            
        }
        
        if ([responseObject[@"message"] isEqualToString:@"Invalid Reset Code"]) {
            title = @"Oops";
            successMsg = @"Fail to reset code";
            [self showSuccessAlert];
            
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
        
        
        
        
        
        
    }];

}


- (BOOL) validEmail:(NSString*) emailString {
    
    if([emailString length]==0){
        return NO;
    }
    
    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    
    // NSLog(@"%i", regExMatches);
    if (regExMatches == 0) {
        return NO;
    } else {
        return YES;
    }
    
    
}


-(void) showSuccessAlert {
    
    UIAlertController *alertSuccess = [UIAlertController alertControllerWithTitle:title message:successMsg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
       // NSLog(@"Email Sent!");
        //email sending
    }];
    
    [alertSuccess addAction:ok];
    [self presentViewController:alertSuccess animated:YES completion:nil];

}

-(void) showFailedAlert {
    
    UIAlertController *alertFail = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Please enter registered email address!" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
        NSLog(@"Fail Alert for Forgot Password");

    }];
    
    [alertFail addAction:ok];
    [self presentViewController:alertFail animated:YES completion:nil];
    
}

@end
