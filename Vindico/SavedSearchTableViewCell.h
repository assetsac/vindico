//
//  SavedSearchTableViewCell.h
//  Vindico
//
//  Created by Belinda Natividad on 6/8/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListSavedSearch.h"

@interface SavedSearchTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewParent;

@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UITextView *txtSavedAve;
@property (weak, nonatomic) IBOutlet UITextView *txtHeaderFields;
@property (weak, nonatomic) IBOutlet UITextView *txtSubHeaderFields;

-(void) setData:(ListSavedSearch *) itemSavedSearch;
//-(void) setData:(ListSavedSearch *) itemSavedSearch aveSection:(ListSavedSearch *)average;

@property (weak, nonatomic) IBOutlet UILabel *lblSubheader;
@property (weak, nonatomic) IBOutlet UILabel *lblSubheaderValue;



@end
