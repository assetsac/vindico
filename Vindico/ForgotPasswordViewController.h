//
//  ForgotPasswordViewController.h
//  Vindico
//
//  Created by Belinda Natividad on 5/10/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "RequestManager.h"

@interface ForgotPasswordViewController : UIViewController <MFMailComposeViewControllerDelegate> {
    MFMailComposeViewController *forgotMailCompose;

}
@property (weak, nonatomic) IBOutlet UITextField *txtEmailAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtResetCode;
@property (weak, nonatomic) IBOutlet UITextField *txtResetPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtResetConfirmPassword;

- (IBAction)btnVerify:(id)sender;
- (IBAction)btnChangePassword:(id)sender;

@end
