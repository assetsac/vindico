//
//  ReportListSavedSearch.m
//  Vindico
//
//  Created by Belinda Natividad on 6/16/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "ReportListSavedSearch.h"
#import "Brand.h"

@implementation ReportListSavedSearch

-(instancetype)initWithData:(NSString*) searchLocationId locationName:(NSString*) searchLocationname brandId:(NSString*) searchBrandId brandName:(NSString*) searchBrandName areaRange:(NSString*) searchAreaRange searchGroups:(NSString*) searchGroupFilter listGroups:(NSArray*) searchListGroups {
    
    if(self = [super init]) {
        _locId      = searchLocationId;
        _locName    = searchLocationname;
        _brandId    = searchBrandId;
        _brandName  = searchBrandName;
        _areaRange  = searchAreaRange;
        _searchGroup = searchGroupFilter;
        //[self.parseGroup:searchListGroups];
        
        NSLog(@"MODEL %@ %@ %@ %@ %@ %@",_locId,_locName,_brandId,_brandName,_areaRange,_searchGroup);
        
        _areaRange = [_areaRange stringByReplacingOccurrencesOfString:@"," withString:@" to "];
        _areaRange = [_areaRange stringByReplacingOccurrencesOfString:@"[" withString:@""];
        _areaRange = [_areaRange stringByReplacingOccurrencesOfString:@"]" withString:@""];
        
    
        
    }
    return self;
}

- (void)parseGroup:(NSArray*) brands{
    self.groups = [NSMutableArray array];
    for(NSDictionary *data in brands){
        Brand *brand = [[Brand alloc]initWithBrandData:data];
        [self.groups addObject:brand];
    }
}



@end
