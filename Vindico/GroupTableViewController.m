//
//  GroupTableViewController.m
//  Vindico
//
//  Created by Belinda Natividad on 5/24/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "GroupTableViewController.h"
#import "SearchViewController.h"
#import "RequestManager.h"
#import "GroupItem.h"

@interface GroupTableViewController ()  <UIPopoverControllerDelegate> {
    
    AFHTTPRequestOperationManager *manager;
    SearchViewController *rootView;
}


@end

@implementation GroupTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _groupNameList = [[NSMutableArray alloc]init];
    
     rootView  = [[SearchViewController alloc]init];
    
//    self.selectedGroupIdA = 1;
//    self.selectedGroupIdB = 2;
//    self.selectedGroupIdC = 3;
   
    [self getGroupListData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - API Requests

-(void) getGroupListData {
    
    
    NSString *getUrl = [NSString stringWithFormat:@"%@get-groups?selected=[%ld,%ld,%ld]",kBaseUrl,(long)self.selectedGroupIdA,self.selectedGroupIdB,self.selectedGroupIdC];
    //http://staging.straightarrowasset.com/vindico/public/api/v1/get-groups?selected=[1,2,3]

    
    _accountToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    
    manager = [AFHTTPRequestOperationManager manager];
    [RequestManager callAPI];
    [manager.requestSerializer setValue:_accountToken forHTTPHeaderField:@"X-Auth-Token"];
    NSLog(@"Group table account token %@",_accountToken);
    
//    NSString *grpIds = [NSString stringWithFormat:@""];
//    NSDictionary *paramGroup = @{@"email":grpIds};
    [manager GET:getUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSArray *arrayOfGroups = responseObject[@"groups"];
        NSLog(@"arrayOfGroups: %@", arrayOfGroups);
        
        NSMutableArray *arrayOfGroupItemObject = [NSMutableArray new];
        
        for (NSDictionary *dictItem in arrayOfGroups) {
            GroupItem *groupItem = [[GroupItem alloc] init];
            groupItem.groupId = [dictItem[@"id"] integerValue];
            groupItem.groupName = dictItem[@"group_name"];
            
            
            [self.groupNameList addObject:groupItem];
            
            
        }
        
        [self.tableView reloadData];
      
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.groupNameList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"groupName" forIndexPath:indexPath];
    
    
    GroupItem *groupItem = [self.groupNameList objectAtIndex:indexPath.row];
    cell.textLabel.text = groupItem.groupName;
//    if (indexPath.row) {
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//    }
    //cell.accessoryType = UITableViewCellAccessoryCheckmark;
//    cell.accessoryType = UITableViewCellAccessoryCheckmark;
//    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.selectedGroupBlock) {
        self.selectedGroupBlock([_groupNameList objectAtIndex:indexPath.row]);
        [_groupNameList removeObjectAtIndex:indexPath.row];
        
     //   NSDictionary *groupName = [_groupNameList objectAtIndex:indexPath.row];
//        UITableViewCell *cellGroupName = [tableView cellForRowAtIndexPath:indexPath];
//
//        if (cellGroupName.accessoryType == UITableViewCellSelectionStyleNone) {
//            cellGroupName.accessoryType = UITableViewCellAccessoryCheckmark;
//        } else {
//            cellGroupName.accessoryType = UITableViewCellSelectionStyleNone;
//        }
    }
    
//    NSIndexPath *selectedIndexPath = [tableView indexPathForSelectedRow];
//    NSLog(@"%@",selectedIndexPath);
    
//    GroupItem *groupItem = [self.groupNameList objectAtIndex:indexPath.row];
//    NSLog(@"SELECTED GROUP ID: %ld", groupItem.groupId);
//    
//    id selectedObject = [self.groupNameList objectAtIndex:indexPath.row];
//    NSInteger *index = [self.groupNameList indexOfObject:selectedObject];
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSIndexPath *oldIndex = [self.tableView indexPathForSelectedRow];
    [self.tableView cellForRowAtIndexPath:oldIndex].accessoryType = UITableViewCellAccessoryNone;
    [self.tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    return indexPath;
}

#pragma mark - Navigation


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"toGroupNameSegue"]) {
        ((SearchViewController *)[segue destinationViewController]).selectedCellText = _selectedCellText;
        
    }
    
}






@end
