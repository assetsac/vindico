//
//  ReportListTableViewController.h
//  Vindico
//
//  Created by Belinda Natividad on 6/15/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListSavedSearch.h"
#import "ListSavedReports.h"
#import "SavedSearchViewController.h"

@interface ReportListTableViewController : UITableViewController <UITableViewDelegate,UITableViewDataSource,SavedSearchViewControllerDelegate>

@property (strong,nonatomic) NSMutableArray *contentReportItems;

@property (strong, nonatomic) ListSavedReports *reportsModel;


@end
