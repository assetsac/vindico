//
//  GroupItem.m
//  Vindico
//
//  Created by Belinda Natividad on 6/17/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "GroupItem.h"

@implementation GroupItem

-(instancetype)initGroupItemWithId:(NSInteger)groupId andGroupName:(NSString *)groupName {
    
    self = [super init];
    
    if (self) {
        _groupId = self.groupId;
        _groupName = self.groupName;
    }
    
    return self;
}

-(instancetype)init {
    return [self initGroupItemWithId:0 andGroupName:@""];
}

@end
