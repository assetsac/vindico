//
//  Brand.m
//  Vindico
//
//  Created by Belinda Natividad on 5/19/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "Brand.h"

@implementation Brand

- (instancetype)initWithBrandData:(NSDictionary*) data{
    
//    NSMutableArray *firstColList = [[NSMutableArray alloc]init];
    
    
    if(self = [super init]){
        self.brandId        = data[@"data"][0][@"id"];
        self.brandValue     = data[@"data"][0][@"value"];
        //self.brandValue     = [data[@"data"][0][@"value"] floatValue];
        self.brandItemId    = data[@"item_id"];
        self.brandArea      = data[@"area"];
        self.brandName      = data[@"brand_name"];
        self.brandLocation  = data[@"location"];
        self.brandRow       = [data[@"row"] integerValue];
    }
    
//    [firstColList addObject:[NSNumber numberWithFloat:_brandValue]];
//    
 //   NSLog(@"%2f",_brandValue);
//    NSLog(@"First Column: %@",firstColList);
    return self;
}

@end
