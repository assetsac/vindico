//
//  LocationList.h
//  Vindico
//
//  Created by Belinda Natividad on 8/22/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationList : NSObject

@property(strong, nonatomic) NSString  *locationSearchID;
@property(nonatomic, strong) NSString   *locationSearchName;

- (instancetype)initWithId:(NSString*) locationId locationName:(NSString*) locName;

@end
