//
//  Item.m
//  Vindico
//
//  Created by Belinda Natividad on 5/19/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "Item.h"
#import "ItemSubgroup.h"

@implementation Item
- (instancetype)initWithId:(NSString*) itemId groupName:(NSString*) name subgroups:(NSArray*) groups {
    if(self = [super init]) {
        self.Id         = itemId;
        self.groupName  = name;
        [self parseSubgroupData:groups];
    }
    return self;
}

- (void)parseSubgroupData:(NSArray*) groups{
    self.subgroupArray = [NSMutableArray array];
    
    for (NSDictionary *data in groups){
        ItemSubgroup *itemSGP = [[ItemSubgroup alloc] initWithId:data[@"sub_group_id"] groupName:data[@"sub_group_name"] average:data[@"average"] brands:data[@"row"]];
        [self.subgroupArray addObject:itemSGP];
    }
}

@end
