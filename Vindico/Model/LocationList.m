//
//  LocationList.m
//  Vindico
//
//  Created by Belinda Natividad on 8/22/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "LocationList.h"

@implementation LocationList

- (instancetype)initWithId:(NSString*) locationId locationName:(NSString*) locName {
    if(self = [super init]) {
        self.locationSearchID    = locationId;
        self.locationSearchName  = locName;
    }
    return self;
}

@end
