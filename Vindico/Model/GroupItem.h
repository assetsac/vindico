//
//  GroupItem.h
//  Vindico
//
//  Created by Belinda Natividad on 6/17/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GroupItem : NSObject

@property (nonatomic) NSInteger groupId;
@property (strong, nonatomic) NSString *groupName;

-(instancetype)initGroupItemWithId:(NSInteger)groupId andGroupName:(NSString *)groupName;

@end
