//
//  BrandList.h
//  Vindico
//
//  Created by Belinda Natividad on 8/22/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BrandList : NSObject

@property(strong, nonatomic) NSString  *brandSearchID;
@property(nonatomic, strong) NSString   *brandSearchName;

- (instancetype)initWithId:(NSString*) brandId brandName:(NSString*) name;

@end
