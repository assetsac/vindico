//
//  ItemSubgroup.m
//  Vindico
//
//  Created by Belinda Natividad on 5/19/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "ItemSubgroup.h"
#import "Brand.h"

@implementation ItemSubgroup
- (instancetype)initWithId:(NSString*) subgroupId groupName:(NSString*) name average:(NSString*) average brands:(NSArray*) groups{
    
    if(self =[super init]){
        self.subGroupId     = subgroupId;
        self.subGroupName   = name;
        self.average        = average;
        [self parseBrand:groups];
    }
    return self;
}

- (void)parseBrand:(NSArray*) brands{
    self.brandArray = [NSMutableArray array];
    for(NSDictionary *data in brands){
        Brand *brand = [[Brand alloc]initWithBrandData:data];
        [self.brandArray addObject:brand];
    }
}

@end
