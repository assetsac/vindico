//
//  ItemSubgroup.h
//  Vindico
//
//  Created by Belinda Natividadon 5/19/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ItemSubgroup : NSObject
@property(nonatomic, strong) NSString       *subGroupId;
@property(nonatomic, strong) NSString       *average;
@property(nonatomic, strong) NSString       *subGroupName;
@property(nonatomic, strong) NSMutableArray *brandArray;
- (instancetype)initWithId:(NSString*) subgroupId groupName:(NSString*) name average:(NSString*) average brands:(NSArray*) groups;

@end
