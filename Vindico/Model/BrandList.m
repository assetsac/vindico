//
//  BrandList.m
//  Vindico
//
//  Created by Belinda Natividad on 8/22/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "BrandList.h"

@implementation BrandList

- (instancetype)initWithId:(NSString*) brandSId brandName:(NSString*) brandSName {
    
    if(self = [super init]) {
        self.brandSearchID    = brandSId;
        self.brandSearchName  = brandSName;
    }
    return self;
    

}

@end
