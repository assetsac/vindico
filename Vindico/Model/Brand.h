//
//  Brand.h
//  Vindico
//
//  Created by Belinda Natividad on 5/19/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Brand : NSObject

@property(nonatomic, strong) NSString *brandId;
@property(nonatomic, strong) NSString *brandItemId;
@property(nonatomic, strong) NSString *brandName;
@property(nonatomic, strong) NSString *brandArea;
@property(nonatomic, strong) NSString *brandLocation;
@property(nonatomic, assign) NSInteger brandRow;
@property(nonatomic, assign) NSString *brandValue;
@property(nonatomic, strong) NSMutableArray *brandData;
//CGFloat

- (instancetype)initWithBrandData:(NSDictionary*) data;

@end
