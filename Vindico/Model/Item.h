//
//  Item.h
//  Vindico
//
//  Created by Belinda Natividad on 5/19/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Item : NSObject
@property(nonatomic, strong) NSString       *Id;
@property(nonatomic, strong) NSString       *groupName;
@property(nonatomic, strong) NSMutableArray *subgroupArray;

- (instancetype)initWithId:(NSString*) itemId groupName:(NSString*) name subgroups:(NSArray*) groups;

@end
