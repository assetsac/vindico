//
//  SearchViewController.h
//  Vindico
//
//  Created by Belinda Natividad on 5/10/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking/AFNetworking.h"
#import "ContentItem.h"
#import "SRContentTableViewCell.h"
#import "GroupTableViewController.h"
#import "SavedSearchViewController.h"

@protocol SearchViewControllerDelegate <NSObject>

//@optional
//-(void) didFinishSaving:(NSDictionary *) details;

@end

@interface SearchViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextFieldDelegate, UIPopoverControllerDelegate, UIPopoverPresentationControllerDelegate, PopOverSelectionDelegate>


@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@property (weak, nonatomic) IBOutlet UILabel *lblSelectedBrand;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectedLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectedArea;
@property (weak, nonatomic) IBOutlet UIView *viewFilter;


@property (nonatomic,weak) id<SearchViewControllerDelegate> delegate;

@property (strong, nonatomic) IBOutlet UITableView *tblSResults;
@property (weak, nonatomic) IBOutlet UITableViewCell *cellSResults;


- (IBAction)btnSearch:(id)sender;

@property (strong, nonatomic) NSMutableArray *contentItems;

@property (strong, nonatomic) NSString *accountToken;
@property(strong,nonatomic) NSString *selectedCellText;

- (IBAction)btnSRGroupName:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnTextGroupName;
@property (weak, nonatomic) IBOutlet UIButton *btnTextGroupName2;
@property (weak, nonatomic) IBOutlet UIButton *btnTextGroupName3;

@property (weak, nonatomic) IBOutlet UITextView *txtViewAve;
- (IBAction)btnSaveReport:(id)sender;

@property (nonatomic, strong) UIPopoverController *popController;

// Properties for accessing the popover and its viewcontroller
@property (strong, nonatomic) UIStoryboardPopoverSegue *currentPopoverSegue;
@property (strong, nonatomic) GroupTableViewController *pvc;
- (IBAction)btnLogout:(id)sender;

@property (strong, nonatomic) NSMutableArray *headerData;
@property (strong, nonatomic) UIPopoverController *popOver;


@property (weak, nonatomic) IBOutlet UILabel *subGroupLabel1;
@property (weak, nonatomic) IBOutlet UILabel *subGroupLabel2;
@property (weak, nonatomic) IBOutlet UILabel *subGroupLabel3;

@property (weak, nonatomic) IBOutlet UIView *viewAverage;
@property (weak, nonatomic) IBOutlet UILabel *aveFirstCol;
@property (weak, nonatomic) IBOutlet UILabel *aveSecCol;
@property (weak, nonatomic) IBOutlet UILabel *aveThirdCol;

@property (weak, nonatomic) IBOutlet UILabel *lblResultCount;


@end
