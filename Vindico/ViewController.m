//
//  ViewController.m
//  Vindico
//
//  Created by Belinda Natividad on 5/10/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking.h"
#import "RequestManager.h"
#import "SearchViewController.h"



@interface ViewController () <SearchViewControllerDelegate> {
    NSString  *accountToken;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    // for testing purposes
//    self.txtUsername.text = @"testuser";
//    self.txtPassword.text = @"123456";
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (IBAction)btnLogin:(id)sender {
    
    NSDictionary *parameters = @{@"username":_txtUsername.text,@"password":_txtPassword.text};
    //NSDictionary *parameters = @{@"username":@"testadmin",@"password":@"123456"};
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager.requestSerializer setValue:accountToken forHTTPHeaderField:@"X-Auth-Token"];
   // [manager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults]stringForKey:accountToken] forHTTPHeaderField:@"X-Auth-Token"];

    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager POST:[NSString stringWithFormat:@"%@auth/login",kBaseUrl] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSDictionary *responseDictionary = responseObject;
        
        if ([responseDictionary[@"message"] isEqualToString:@"Successfully login"]) {
            accountToken = [responseDictionary objectForKey:@"token"];
            [[NSUserDefaults standardUserDefaults] setObject:accountToken forKey:@"token"];
            [[NSUserDefaults standardUserDefaults] synchronize];
           // [[NSUserDefaults standardUserDefaults] setValue:responseDictionary[@"token"] forKey:@"X-Auth-Token"];
            NSLog(@"%@",accountToken);

            [self showSuccessAlert];
        }         

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self showFailLoginAlert];
        
        
        
    }];
    

    
}

- (void) showSuccessAlert {
    
    UIAlertController *alertCtrl = [UIAlertController alertControllerWithTitle:@"Login" message:@"You have successfully login!..." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [self performSegueWithIdentifier:@"mainSearch" sender:self];
    }];
    
    [alertCtrl addAction:ok];
    [self presentViewController:alertCtrl animated:YES completion:nil];
}

- (void) showFailLoginAlert {
    
    UIAlertController *alertCtrl = [UIAlertController alertControllerWithTitle:@"Login" message:@"Wrong username or password" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertCtrl addAction:cancel];
    [self presentViewController:alertCtrl animated:YES completion:nil];
}



#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    if([segue.identifier isEqualToString:@"mainSearch"]) {
//        ((SearchViewController *)[segue destinationViewController]).accountToken = accountToken;
//    }
}

#pragma mark -Keyboard

#define kOFFSET_FOR_KEYBOARD 80.0

-(void)keyboardWillShow {
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide {
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if (sender.tag == 1)
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
@end
