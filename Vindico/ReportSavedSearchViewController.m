//
//  ReportSavedSearchViewController.m
//  Vindico
//
//  Created by Belinda Natividad on 6/16/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "ReportSavedSearchViewController.h"
#import "ReportItemSSTableViewCell.h"
#import "ReportListSavedSearch.h"
#import "AFNetworking/AFNetworking.h"
#import "ListSavedReports.h"
#import "RequestManager.h"
#import "ListSavedSearch.h"
#import "Item.h"
#import "ItemSubgroup.h"
#import "Brand.h"
#import "ListSavedSearch.h"



@interface ReportSavedSearchViewController () {
    AFHTTPRequestOperationManager *manager;
    NSString *getSavedUrl;
    NSString *getSavedReportUrl;
    NSString *getUrl;
    
    NSDictionary *tmpDict;
    
    NSString *filename;
    NSString *downloadUrl;
    
    UIWebView *webview;
    UIBarButtonItem *sendButton;
    
    NSString *urlPdf;
    NSURL *nsurlPdf;
    
    CGFloat labelHeight;
    CGSize labelSize;
    
    
}

@end

@implementation ReportSavedSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self displayResultsSavedSearchFromAPI];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Events
- (IBAction)btnSaveReport:(id)sender {
    NSString *reportId =  [NSString stringWithFormat:@"%@",_mdl.reportId];
    NSLog(@"Report ID to download: %@",reportId);
    
   [self downloadReportfromAPI];

}

#pragma mark - API Requests
-(void) displayResultsSavedSearchFromAPI {
    
    
    
//    getUrl = [NSString stringWithFormat:@"http://www.vrdnet.net/api/public/api/v1/list-of-save-report"];
    
    _accountToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    
    manager = [AFHTTPRequestOperationManager manager];
    [RequestManager callAPI];
    [manager.requestSerializer setValue:_accountToken forHTTPHeaderField:@"X-Auth-Token"];

    
    [manager GET:[NSString stringWithFormat:@"%@list-of-save-report",kBaseUrl] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // _contentItems = responseObject;
        
        if(_contentReportSearchItems){
            [_contentReportSearchItems removeAllObjects];
        }else{
            _contentReportSearchItems = [NSMutableArray array];
        }
        
        for(NSDictionary *data in responseObject) {
            ListSavedReports *itemRSearch = [[ListSavedReports alloc] initWithData:data[@"id"] reportName:data[@"name"] createdDate:data[@"created_at"] listReports:data[@"report"]];
            [self.contentReportSearchItems addObject:itemRSearch];
            NSLog(@"Item Search %@",itemRSearch.reportName);
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tblReportItemSavedSearch reloadData];
        });
        
        [self.tblReportItemSavedSearch reloadData];
        
        //check array
        //        NSLog(@"ARRAY: %@ - %ld", self.contentSearchItems, self.contentSearchItems.count);
        
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}

-(void) downloadReportfromAPI {
    
//    downloadUrl = [NSString stringWithFormat:@"http://www.vrdnet.net/api/public/api/v1/get-report"];
    
    _accountToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    
    manager = [AFHTTPRequestOperationManager manager];
    [RequestManager callAPI];
    [manager.requestSerializer setValue:_accountToken forHTTPHeaderField:@"X-Auth-Token"];
    
    NSDictionary *parameters = @{@"report_id":_mdl.reportId};
    [manager POST:[NSString stringWithFormat:@"%@get-report",kBaseUrl] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        
        if(self.reportContent){
            [self.reportContent removeAllObjects];
        } else {
            self.reportContent = [NSMutableArray array];
        }
        
        
        NSNumber *isSuccessNumber = (NSNumber *)[responseObject valueForKey:@"success"];
        
           
             if ([isSuccessNumber boolValue] == YES) {
//                NSString *webUrl = [responseObject valueForKey:@"url"];
//                NSLog(@"%@",webUrl);
//                 UIAlertController *alertSuccess = [UIAlertController alertControllerWithTitle:@"Success" message:@"View Report?" preferredStyle:UIAlertControllerStyleAlert];
//                 
//                 UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                     [self dismissViewControllerAnimated:YES completion:nil];
                 
                     webview=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,self.view.bounds.size.height)];
                     urlPdf=[responseObject valueForKey:@"url"];
                     nsurlPdf=[NSURL URLWithString:urlPdf];
                     NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurlPdf];
                     [webview loadRequest:nsrequest];
                     [self.view addSubview:webview];
                     
                     
                     sendButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(sharePdf)];
  
                     
                     self.navigationItem.rightBarButtonItem = sendButton;

//                 }];
                 
//                 UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//                     [self dismissViewControllerAnimated:YES completion:nil];
//                     // NSLog(@"Email Sent!");
//                     //email sending
//                 }];

                 
//                 [alertSuccess addAction:ok];
                 //[alertSuccess addAction:cancel];
//                 [self presentViewController:alertSuccess animated:YES completion:nil];
                 

             }
       
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];


}

-(void) sharePdf {
    
//    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share" message:@"Test" preferredStyle:UIAlertControllerStyleActionSheet];
    NSString *textToShare = @"Testing -- Look at this awesome website for aspiring iOS Developers!";
    NSURL *myWebsite = [NSURL URLWithString:urlPdf];

    
    NSArray *objectsToShare = @[textToShare, myWebsite];
   // NSArray *appsToShare = @[UIActivityTypePostToFacebook,UIActivityTypePostToTwitter];
  //  NSArray *applicationActivities = @[UIActi];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    activityVC.popoverPresentationController.sourceView = webview;
    activityVC.popoverPresentationController.barButtonItem = self.navigationItem.rightBarButtonItem;
    
    NSArray *excludeActivities = @[UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll, UIActivityTypePostToFlickr, UIActivityTypePostToVimeo,UIActivityTypePostToWeibo];
    

    activityVC.excludedActivityTypes = excludeActivities;
    
   
    
    
   [self presentViewController:activityVC animated:YES completion:nil];
   // [self.pop]

}

#pragma mark - Table view data source


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //ListSavedReports *itemR = self.contentReportSearchItems[0];
    //ListSavedSearch *reportSS = _m.reportsArray[0];
    return [_mdl.reportsArray count];
}  

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ReportItemSSTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reportItemSS"];
    if (cell == nil) {
        cell = [[ReportItemSSTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reportItemSS"];
    }
    
    NSMutableString *valuesHeaderColumns = [[NSMutableString alloc] init];
    NSMutableString *valuesSubHeaderColumns = [[NSMutableString alloc] init];
    NSMutableString *valuesAverageColumns = [[NSMutableString alloc] init];
    
    NSString *valuesHeader;
    
//    NSMutableAttributedString *valuesSubHeaderColumns = [[NSMutableAttributedString alloc] init];

    ListSavedSearch *rSSearch = [_mdl.reportsArray objectAtIndex:indexPath.row];

    for (Item *group in rSSearch.groups) {
        NSString *groupName = group.groupName;
    
        [valuesSubHeaderColumns appendString:[NSString stringWithFormat:@"%@ \n\n", groupName]];
        [valuesAverageColumns appendString:[NSString stringWithFormat:@"\n\n"]];

        for (ItemSubgroup *subgroup in group.subgroupArray) {
            NSString *subGroupName  = subgroup.subGroupName;
            NSString *subAve        = subgroup.average;
            [valuesSubHeaderColumns appendString:[NSString stringWithFormat:@"%@ \n",subGroupName]];
            [valuesAverageColumns appendString:[NSString stringWithFormat:@"%@ \n", subAve]];
           
        }
        [valuesSubHeaderColumns appendString:[NSString stringWithFormat:@"\n"]];
        [valuesAverageColumns appendString:[NSString stringWithFormat:@"\n"]];
    }
    
    
    cell.lblReportItemSSHeader.text = [NSString stringWithFormat:@"%@ - %@ - %@",rSSearch.brandName,rSSearch.locName,rSSearch.areaRange];
    cell.lblRSubHeader.text         = valuesSubHeaderColumns;
    cell.lblRSubHeaderVal.text      = valuesAverageColumns;
    
//    cell.lblSubHeader.text        = valuesHeaderColumns;
//    cell.txtSubGroupHeader.text     = valuesSubHeaderColumns;
////    cell.txtAverage.text            = valuesAverageColumns;
    
    labelSize = [valuesSubHeaderColumns sizeWithAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14.0f]}];
    labelHeight = labelSize.height;
    NSLog(@"%.2f",labelHeight);
    
    return cell;

    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    NSInteger *heightCell;
    
//    return (long)heightCell;
    
    return labelHeight + 50;

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
