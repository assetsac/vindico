//
//  ListSavedSearch.h
//  Vindico
//
//  Created by Belinda Natividad on 6/14/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ListSavedSearch : NSObject

@property(nonatomic, strong) NSString       *locId;
@property(nonatomic, strong) NSString       *locName;
@property(nonatomic, strong) NSString       *brandId;
@property(nonatomic, strong) NSString       *brandName;
@property(nonatomic, strong) NSString       *areaRange;
@property(nonatomic, strong) NSString       *searchGroup;
@property(nonatomic, strong) NSMutableArray        *groups;

- (instancetype)initWithData:(NSString*) searchLocationId locationName:(NSString*) searchLocationname brandId:(NSString*) searchBrandId brandName:(NSString*) searchBrandName areaRange:(NSString*) searchAreaRange searchGroups:(NSString*) searchGroupFilter listGroups:(NSArray*) searchListGroups;

@end
