//
//  ReportListTableViewController.m
//  Vindico
//
//  Created by Belinda Natividad on 6/15/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "ReportListTableViewController.h"
#import "AFNetworking/AFNetworking.h"
#import "ReportSavedSearchViewController.h"
#import "RequestManager.h"
#import "ListSavedSearch.h"


@interface ReportListTableViewController () {
    AFHTTPRequestOperationManager *manager;
    NSString *accountToken;
    
    NSString *reportListURL;
    ListSavedSearch *itemToView;
    
    UIAlertController *alertWarning;
    NSString *alertWarningTitle;
    NSString *alertWarningMsg;
    
    
}

@end

@implementation ReportListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _contentReportItems = [[NSMutableArray alloc]init];
    
    [self callReportListAPI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - API Requests

-(void) callReportListAPI {

//  reportListURL = @"http://www.vrdnet.net/api/public/api/v1/list-of-save-report";
    
    accountToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    
    manager = [AFHTTPRequestOperationManager manager];
    [RequestManager callAPI];
    [manager.requestSerializer setValue:accountToken forHTTPHeaderField:@"X-Auth-Token"];
    [manager GET:[NSString stringWithFormat:@"%@list-of-save-report",kBaseUrl] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if(_contentReportItems){
            [_contentReportItems removeAllObjects];
        }else{
            _contentReportItems = [NSMutableArray array];
        }
        
        for(NSDictionary *data in responseObject){
            ListSavedReports *item = [[ListSavedReports alloc] initWithData:data[@"id"] reportName:data[@"name"] createdDate:data[@"created_at"] listReports:data[@"report"]];
            [_contentReportItems addObject:item];
            NSLog(@"Report Item %@",item);
            
        }
        
        [self.tableView reloadData];
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
        alertWarningTitle   = @"Warning";
        
        if (error.code == NSURLErrorTimedOut) {
            alertWarningMsg     = @"Connection Timed Out";
        } else if (error.code == NSURLErrorNotConnectedToInternet) {
            alertWarningMsg     = @"Not connected to internet";
        } else {
            alertWarningMsg     = @"Some problem occured.";
        }
        
        [self callWarningAlert];
        
    }];
    
}

#pragma mark - Alert Controller
-(void) callWarningAlert {
    
    alertWarning = [UIAlertController alertControllerWithTitle:alertWarningTitle message:alertWarningMsg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertWarning addAction:ok];
    [self presentViewController:alertWarning animated:YES completion:nil];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _contentReportItems.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reportItemCell" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reportItemCell"];
    }
    
    ListSavedReports *item = [_contentReportItems objectAtIndex:indexPath.row];
    
    
    cell.textLabel.text = item.reportName;
    cell.detailTextLabel.text = item.reportCreatedDate;

    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    itemToView = nil;
    itemToView = _contentReportItems[[indexPath row]];
    NSLog(@"fdsfsdf %@",itemToView);
    [self performSegueWithIdentifier:@"toReportSavedSearch" sender:self];
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"toReportSavedSearch"]){
//        NSIndexPath *path = [self.tableView indexPathForSelectedRow];
//        NSLog(@"%@",path);
//        itemToView = [_contentReportItems objectAtIndex:path.row];
        [(ReportSavedSearchViewController *)segue.destinationViewController setDelegate:self];
        [(ReportSavedSearchViewController *)segue.destinationViewController setMdl:itemToView];
    }
}


@end
