//
//  SRContentTableViewCell.h
//  Vindico
//
//  Created by Belinda Natividad on 5/17/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContentItem.h"
#import "Brand.h"

@interface SRContentTableViewCell : UITableViewCell
//@property (weak, nonatomic) IBOutlet UILabel *lblSRContent;
//@property (weak, nonatomic) IBOutlet UILabel *lblSRContentB;

@property (weak, nonatomic) IBOutlet UILabel *lblIndex;

@property (weak, nonatomic) IBOutlet UILabel *lblFirstCol;
@property (weak, nonatomic) IBOutlet UILabel *lblSecCol;
@property (weak, nonatomic) IBOutlet UILabel *lblThirdCol;


@property (weak, nonatomic) NSString *ElecCol;
@property (weak, nonatomic) NSString *GasCol;
@property (weak, nonatomic) NSString *airCol;
@property (weak, nonatomic) NSString *VentCol;
@property (weak, nonatomic) NSString *fireCol;
@property (weak, nonatomic) NSString *itCol;

-(void) setElectricalData:(Brand*) brand otherSection: (Brand*) otherBrand;
-(void) setGasData:(Brand*) brand otherSection: (Brand*) otherBrand;
-(void) setAirconditioningData:(Brand*) brand otherSection: (Brand*) otherBrand otherSecSection:(Brand*) otherBrandSec otherThirdSection:(Brand*) otherBrandThird;
-(void) setVentilationData:(Brand*) brand otherSection: (Brand*) otherBrand otherSecSection:(Brand*) otherBrandSec otherThirdSection:(Brand*) otherBrandThird otherFourthSection:(Brand*) otherBrandFourth otherFifthSection:(Brand*) otherBrandFifth;
-(void) setFirefightingData:(Brand*) brand otherSection: (Brand*) otherBrand otherSecSection:(Brand*) otherBrandSec otherThirdSection:(Brand*) otherBrandThird;
-(void) setITData:(Brand*) brand;


@end
