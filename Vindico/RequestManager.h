//
//  RequestManager.h
//  Vindico
//
//  Created by Belinda Natividad on 5/10/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking/AFNetworking.h"


@interface RequestManager : NSObject


+(void) callAPI;

//- (instancetype)callAPI:(NSString*) urlString dataParam:(NSString*) param success:(void(^)(AFHTTPRequestOperation *operation, id responseObject)) block;

@end
