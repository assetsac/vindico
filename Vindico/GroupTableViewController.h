//
//  GroupTableViewController.h
//  Vindico
//
//  Created by Belinda Natividad on 5/24/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupItem.h"

@protocol PopOverSelectionDelegate <NSObject>

- (void)popOverItemSelected:(NSString *)selectedGroupItem;
-(void)didTap;

@end

@interface GroupTableViewController : UITableViewController

@property(strong,nonatomic) NSMutableArray *groupNameList;
@property(strong,nonatomic) NSString *selectedCellText;
@property (nonatomic,weak) id <PopOverSelectionDelegate> delegate;

@property (strong, nonatomic) NSString *accountToken;

//@property(copy)void (^selectedGroupBlock)(NSString *groupName);
@property(copy)void (^selectedGroupBlock)(GroupItem *groupName);

@property (nonatomic) NSInteger selectedGroupIdA;
@property (nonatomic) NSInteger selectedGroupIdB;
@property (nonatomic) NSInteger selectedGroupIdC;

@end
