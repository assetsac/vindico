//
//  SavedSearchViewController.h
//  Vindico
//
//  Created by Belinda Natividad on 6/8/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchViewController.h"
#import "SavedSearchTableViewCell.h"
//#import "listSavedReports.h"

@protocol SavedSearchViewControllerDelegate <NSObject>

@end


@interface SavedSearchViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tblSavedSearch;
@property (strong, nonatomic) NSMutableArray *savedSearch;
@property (strong, nonatomic) NSMutableArray *tmpSaveSearchList;
@property (strong, nonatomic) NSString *getUrl;

@property (strong, nonatomic) NSMutableArray *contentSearchItems;

@property (weak, nonatomic) id<SavedSearchViewControllerDelegate> delegate;
@property (strong, nonatomic,nullable) ListSavedSearch *mdl;

@property (strong, nonatomic) NSString *accountToken;

- (IBAction)btnAddToSearch:(id)sender;
- (IBAction)btnSaveReport:(id)sender;

@end
