//
//  AppDelegate.h
//  Vindico
//
//  Created by Belinda Natividad on 5/10/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

