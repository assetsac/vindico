//
//  ContentItem.h
//  Vindico
//
//  Created by Belinda Natividad on 5/16/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContentItem : NSObject

@property (nonatomic, strong) NSString *itemName;
@property (nonatomic, strong) NSString *itemHeader;
@property (nonatomic, strong) NSString *itemSubGroup;

@property (nonatomic, strong) NSString *itemFirstColA;
@property (nonatomic, strong) NSString *itemFirstColB;
@property (nonatomic, strong) NSString *itemSecColA;
@property (nonatomic, strong) NSString *itemSecColB;
@property (nonatomic, strong) NSString *itemSecColC;
@property (nonatomic, strong) NSString *itemSecColD;
@property (nonatomic, strong) NSString *itemThirdColA;
@property (nonatomic, strong) NSString *itemThirdColB;
@property (nonatomic, strong) NSString *itemThirdColC;
@property (nonatomic, strong) NSString *itemThirdColD;






+ (instancetype)contentHeaderItemWithDictionary:(NSDictionary *)dictionary;
+ (instancetype)contentSubHeaderItemWithDictionary:(NSDictionary *)dictionary;
+ (instancetype)contentItemWithDictionary:(NSDictionary *)dictionary;

@end
