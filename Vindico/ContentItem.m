//
//  ContentItem.m
//  Vindico
//
//  Created by Belinda Natividad on 5/16/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "ContentItem.h"

@implementation ContentItem

+ (instancetype)contentItemWithDictionary:(NSDictionary *)dictionary {
    ContentItem *item = [[ContentItem alloc]init];
    item.itemName = dictionary[@"group_name"];
    item.itemFirstColA = dictionary[@"sub_groups"][0][@"row"][0][@"data"][0][@"value"];
    item.itemFirstColB = dictionary[@"value"];
//    item.itemSecColA = dictionary[@""];
//    item.itemSecColB = dictionary[@""];
//    item.itemSecColC = dictionary[@""];
//    item.itemSecColD = dictionary[@""];
//    item.itemThirdColA = dictionary[@""];
//    item.itemThirdColB = dictionary[@""];
//    item.itemThirdColC = dictionary[@""];
//    item.itemThirdColD = dictionary[@""];
    
    return item;
    
}

+ (instancetype)contentHeaderItemWithDictionary:(NSDictionary *)dictionary {
    ContentItem *headerItem = [[ContentItem alloc]init];
    headerItem.itemHeader = dictionary[@"group_name"];
    
    return headerItem;

}

+ (instancetype)contentSubHeaderItemWithDictionary:(NSDictionary *)dictionary {
    ContentItem *subHeaderItem = [[ContentItem alloc]init];
    subHeaderItem.itemSubGroup = dictionary[@"sub_groups"];
    
    return subHeaderItem;
    
}

@end
