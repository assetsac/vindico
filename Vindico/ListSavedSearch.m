//
//  ListSavedSearch.m
//  Vindico
//
//  Created by Belinda Natividad on 6/14/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "ListSavedSearch.h"
#import "Item.h"

@implementation ListSavedSearch

-(instancetype)initWithData:(NSString*) searchLocationId locationName:(NSString*) searchLocationname brandId:(NSString*) searchBrandId brandName:(NSString*) searchBrandName areaRange:(NSString*) searchAreaRange searchGroups:(NSString*) searchGroupFilter listGroups:(NSArray*) searchListGroups {
    
    if(self = [super init]) {
        _locId      = searchLocationId;
        _locName    = searchLocationname;
        _brandId    = searchBrandId;
        _brandName  = searchBrandName;
        _areaRange  = searchAreaRange;
        _searchGroup = searchGroupFilter;
        [self parseItem:searchListGroups];
        
        NSLog(@"MODEL %@ %@ %@ %@ %@ %@",_locId,_locName,_brandId,_brandName,_areaRange,_searchGroup);
        
        _areaRange = [_areaRange stringByReplacingOccurrencesOfString:@"," withString:@" to "];
        _areaRange = [_areaRange stringByReplacingOccurrencesOfString:@"[" withString:@""];
        _areaRange = [_areaRange stringByReplacingOccurrencesOfString:@"]" withString:@""];
        
    }
    return self;
}

//- (void)parseItem {
// Item *item = [[Item alloc]init];
//
//}

- (void)parseItem:(NSArray*) items{
    self.groups = [NSMutableArray array];
    for(NSDictionary *data in items){
       
        Item *item = [[Item alloc]initWithId:data[@"group_id"] groupName:data[@"group_name"] subgroups:data[@"sub_groups"]];
       [self.groups addObject:item];
        NSLog(@"%@",item.groupName);
    }
}



@end
