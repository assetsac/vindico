//
//  SRContentTableViewCell.m
//  Vindico
//
//  Created by Belinda Natividad on 5/17/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "SRContentTableViewCell.h"

@implementation SRContentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


//Electrical - 2 Subcolumns
-(void) setElectricalData:(Brand*) brand otherSection: (Brand*) otherBrand {
   // self.lblIndex.text      = [NSString stringWithFormat:@"%lu",brand.brandRow];
    _ElecCol = [NSString stringWithFormat:@"%@| %@ ",brand.brandValue, otherBrand.brandValue];

}

//Gas - 2 SubColumns
-(void) setGasData:(Brand*) brand otherSection: (Brand*) otherBrand {
    _GasCol = [NSString stringWithFormat:@"%@ | %@",brand.brandValue, otherBrand.brandValue];

}

//HVAC Air Conditioning - 4 SubColumns
-(void) setAirconditioningData:(Brand*) brand otherSection: (Brand*) otherBrand otherSecSection:(Brand*) otherBrandSec otherThirdSection:(Brand*) otherBrandThird {
    _airCol = [NSString stringWithFormat:@"%@ | %@ | %@ | %@",brand.brandValue, otherBrand.brandValue,otherBrandSec.brandValue,otherBrandThird.brandValue];
    
}

//HVAC Ventilation - 6 SubColumns
-(void) setVentilationData:(Brand*) brand otherSection: (Brand*) otherBrand otherSecSection:(Brand*) otherBrandSec otherThirdSection:(Brand*) otherBrandThird otherFourthSection:(Brand*) otherBrandFourth otherFifthSection:(Brand*) otherBrandFifth {
    
    _VentCol = [NSString stringWithFormat:@"%@ | %@ | %@ | %@ | %@ | %@",brand.brandValue, otherBrand.brandValue,otherBrandSec.brandValue, otherBrandThird.brandValue,otherBrandFourth.brandValue, otherBrandFifth.brandValue];
    
}

//Firefighting- 4 SubColumns
-(void) setFirefightingData:(Brand*) brand otherSection: (Brand*) otherBrand otherSecSection:(Brand*) otherBrandSec otherThirdSection:(Brand*) otherBrandThird {
    _fireCol = [NSString stringWithFormat:@"%@ | %@ | %@ | %@",brand.brandValue, otherBrand.brandValue,otherBrandSec.brandValue, otherBrandThird.brandValue];
}

// IT - 1 SubColumns
-(void) setITData:(Brand*) brand {
    _itCol = [NSString stringWithFormat:@"%@",brand.brandValue];
}

//Plumbing -5 SubColumns




@end
