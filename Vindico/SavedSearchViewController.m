//
//  SavedSearchViewController.m
//  Vindico
//
//  Created by Belinda Natividad on 6/8/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "SavedSearchViewController.h"
#import "ListSavedSearch.h"
#import "Item.h"
#import "ItemSubgroup.h"
#import "Brand.h"
#import "RequestManager.h"


@interface SavedSearchViewController () <UITableViewDelegate,UITableViewDataSource> {
    AFHTTPRequestOperationManager *manager;
    NSString *accountToken;
    NSString *getSavedUrl;
    NSString *getSavedReportUrl;
    NSString *getUrl;
    
    NSDictionary *tmpDict;
    
    NSString *filename;
    
    CGFloat labelHeight;
    CGSize labelSize;
    
   
}

@end

@implementation SavedSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tblSavedSearch.dataSource = self;
    self.tblSavedSearch.delegate = self;
    
    
    //_savedSearch = [[NSMutableArray alloc]ini];
    _tmpSaveSearchList = [[NSMutableArray alloc]init];
    //_tmpSaveSearchList = _savedSearch;
    
    NSLog(@"Saved Seach from another view controller %@",_savedSearch);
    
    [self.tblSavedSearch reloadData];
    
    [self displaySavedSearchFromAPI];
    [self backViewController];
    
    self.navigationItem.hidesBackButton = YES;
    
    
//    self.tblSavedSearch.estimatedRowHeight = 140;
//    self.tblSavedSearch.rowHeight = UITableViewAutomaticDimension;
    

   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
     [self.tblSavedSearch reloadData];
}

- (UIViewController *)backViewController
{
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
//   // numberOfViewControllers = numberOfViewControllers - 2;
//    
//    if (numberOfViewControllers < 2)
//        return nil;
//    else
//        //UIViewController *prevC = [(UIViewController *) [self.navigationController.viewControllers objectAtIndex:numberOfViewControllers - 2] ];
//        return [self.navigationController.viewControllers objectAtIndex:numberOfViewControllers - 2];
    
//    NSArray *viewControllers = self.navigationController.viewControllers;
//    UIViewController *rootViewController =[viewControllers objectAtIndex:viewControllers.count - 2];
//    NSLog(@"%@",NSStringFromClass([self rootViewController]);
//    return rootViewController;
    UIViewController *allViewController = self.navigationController.viewControllers[numberOfViewControllers - 2];
    Class previousVCClass = [allViewController class];
    NSString *className = NSStringFromClass(previousVCClass);
    NSLog (@"%@",className);
    return className;
    
}

#pragma mark - Lazy Init
//-(NSMutableArray *)contentSearchItems {
//    if (!_contentSearchItems) {
//        _contentSearchItems = [NSMutableArray new];
//    }
//    
//    return _contentSearchItems;
//}

#pragma mark - Button Events

- (IBAction)btnAddToSearch:(id)sender {
    accountToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    
    manager = [AFHTTPRequestOperationManager manager];
    [RequestManager callAPI];
    [manager.requestSerializer setValue:accountToken forHTTPHeaderField:@"X-Auth-Token"];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)btnSaveReport:(id)sender {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Save Report" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Title";
    }];
    
//    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
//        textField.placeholder = @"Description";
//    }];
    
    UIAlertAction *successAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UITextField *saveTitle = alertController.textFields.firstObject;
        //UITextField *saveDesc = alertController.textFields.lastObject;
        
        filename = saveTitle.text;
        
        NSLog(@"Filename : %@",filename);
        
        [self SaveReportToAPI];
       // toReportListSegue
        [self performSegueWithIdentifier:@"toReportListSegue" sender:self];
//        [self.navigationController popViewControllerAnimated:YES];
        
        
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)  {
        [self dismissViewControllerAnimated:YES completion:nil];
        NSLog(@"Cancel action");
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:successAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

#pragma mark - API Requests

-(void) SaveReportToAPI {
    
//    getSavedReportUrl =@"http://www.vrdnet.net/api/public/api/v1/save-report";
    //@"http://staging.straightarrowasset.com/vindico/public/api/v1/save-report";
    
    accountToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    
    manager = [AFHTTPRequestOperationManager manager];
    [RequestManager callAPI];
    [manager.requestSerializer setValue:accountToken forHTTPHeaderField:@"X-Auth-Token"];

    NSDictionary *parameters = @{@"name":filename};
    [manager POST:[NSString stringWithFormat:@"%@save-report",kBaseUrl] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"parameters!:%@",parameters);
        NSDictionary *responseDictionary = responseObject;
        
        if ([responseDictionary[@"message"] isEqualToString:@"Report successfully save"]) {
           // [self showSuccessAlert];
             NSLog(@"Successfully save report");
        }
        
        

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    UIAlertController *alertSuccess = [UIAlertController alertControllerWithTitle:@"Success" message:@"Results successfully saved to your report!" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [self performSegueWithIdentifier:@"toReportListSegue" sender:self];
    }];
    
    [alertSuccess addAction:ok];
    [self presentViewController:alertSuccess animated:YES completion:nil];
    
    //[self performSegueWithIdentifier:@"toSaveReport" sender:self];



}

-(void) displaySavedSearchFromAPI {
    
  
//    getUrl = [NSString stringWithFormat:@"http://www.vrdnet.net/api/public/api/v1/list-of-save-search"];
    
    
    accountToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    
    manager = [AFHTTPRequestOperationManager manager];
    [RequestManager callAPI];
    [manager.requestSerializer setValue:accountToken forHTTPHeaderField:@"X-Auth-Token"];
    
    [manager GET:[NSString stringWithFormat:@"%@list-of-save-search",kBaseUrl] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
       // _contentItems = responseObject;
     
        if(_contentSearchItems){
            [_contentSearchItems removeAllObjects];
        }else{
            _contentSearchItems = [NSMutableArray array];
        }
        
            for(NSDictionary *data in responseObject) {
                //ListSavedSearch *itemSearch =  [[ListSavedSearch alloc] initWithId:data[@"group_id"] groupName:data[@"group_name"] subgroups:data[@"sub_groups"]];
                ListSavedSearch *itemSearch =  [[ListSavedSearch alloc] initWithData:data[@"location_id"] locationName:data[@"location_name"] brandId:data[@"brand_id"] brandName:data[@"brand_name"] areaRange:data[@"area_range"] searchGroups:data[@"search_group"] listGroups:data[@"groups"]];
                
                //            [_contentSearchItems addObject:itemSearch];
                [self.contentSearchItems addObject:itemSearch];
                NSLog(@"Item Search %@ - %@",itemSearch, itemSearch.locName);
                
            }
       
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tblSavedSearch reloadData];
        });
        
        [self.tblSavedSearch reloadData];
        
        //check array
//        NSLog(@"ARRAY: %@ - %ld", self.contentSearchItems, self.contentSearchItems.count);
        
       
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

}


     

#pragma mark - Table view data source


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//   return _savedSearch.count;
//    ListSavedSearch *item  = self.contentSearchItems[0];
    //return [item count];
//    return [_contentSearchItems count];
    
    return self.contentSearchItems.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SavedSearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"saveReportCell"];
    if (cell == nil) {
        cell = [[SavedSearchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"saveReportCell"];
    }
    
    ListSavedSearch *itemSaved = [_contentSearchItems objectAtIndex:indexPath.row];
    //Item *itemSearch = itemSaved.groups[];
    //ListSavedSearch *itemSaved = _contentSearchItems[[indexPath section]];
    
    
  //  cell.txtHeaderFields.text = [NSString stringWithFormat:@"%@",]
    cell.lblHeader.text = [NSString stringWithFormat:@"%@ - %@ - %@",itemSaved.brandName,itemSaved.locName,itemSaved.areaRange];
    
    NSMutableString *valuesHeaderColumns = [[NSMutableString alloc] init];
    NSMutableString *valuesSubHeaderColumns = [[NSMutableString alloc] init];
    NSMutableString *valuesAveColumns = [[NSMutableString alloc] init];
    //for (ListSavedSearch *savedSearch in _contentSearchItems) {
        for (Item *itemLSaved in itemSaved.groups) {
            NSString *itemArray = itemLSaved.groupName;
            [valuesSubHeaderColumns appendString:[NSString stringWithFormat:@"%@ \n\n", itemArray]];
            [valuesAveColumns appendString:[NSString stringWithFormat:@"\n\n"]];
            for (ItemSubgroup *iS in itemLSaved.subgroupArray) {
                NSString *itemSubArray  = iS.subGroupName;
                NSString *itemAve       = iS.average;
                [valuesSubHeaderColumns appendString:[NSString stringWithFormat:@"%@ \n", itemSubArray]];
//                [valuesAveColumns appendString:[NSString stringWithFormat:@"%@ | ", itemAve]];
                  [valuesAveColumns appendString:[NSString stringWithFormat:@"%@  \n", itemAve]];

            }
            [valuesSubHeaderColumns appendString:[NSString stringWithFormat:@"\n"]];
            [valuesAveColumns appendString:[NSString stringWithFormat:@"\n"]];

        }
    
    cell.txtHeaderFields.text       = valuesHeaderColumns;
    cell.txtSubHeaderFields.text    = valuesSubHeaderColumns;
//    cell.txtSavedAve.text           = valuesAveColumns;
    
    cell.lblSubheader.text  =   valuesSubHeaderColumns;
    cell.lblSubheaderValue.text = valuesAveColumns;

    
    [cell.lblHeader setUserInteractionEnabled:NO];
    [cell.txtSavedAve setUserInteractionEnabled:NO];
    
    labelSize = [valuesSubHeaderColumns sizeWithAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14.0f]}];
    labelHeight = labelSize.height;
    NSLog(@"%.2f",labelHeight);
   
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return labelHeight + 50;
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
