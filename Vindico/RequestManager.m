//
//  RequestManager.m
//  Vindico
//
//  Created by Belinda Natividad on 5/10/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "RequestManager.h"
#import "AFNetworking/AFNetworking.h"


@implementation RequestManager


+(void) callAPI {
    
    //_accountToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    //NSString *accountToken;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSLog(@"Conection Successful!");
    
    // return accountToken;
}



//- (instancetype)callAPI:(NSString*) urlString dataParam:(NSString*) param success:(void(^)(AFHTTPRequestOperation *operation, id responseObject)) block {
//  if (self = [super init]){
//    
//        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//        [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
//        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//
//        [manager POST:urlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
//            NSLog(@"JSON: %@", responseObject);
//        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            NSLog(@"Error: %@", error);
//        }];
//
//    
//   }
//    return self;
//}
//    return NO;
//}

@end
