//
//  ReportSavedSearchViewController.h
//  Vindico
//
//  Created by Belinda Natividad on 6/16/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportListSavedSearch.h"
#import "ListSavedReports.h"
#import "ListSavedSearch.h"

@protocol ReportSavedSearchViewControllerDelegate <NSObject>

@end

@interface ReportSavedSearchViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblReportItemSavedSearch;

@property (strong, nonatomic) NSMutableArray *contentReportSearchItems;

@property (weak, nonatomic) id<ReportSavedSearchViewControllerDelegate> delegate;
@property (strong, nonatomic,nullable) ListSavedReports *mdl;
//@property (strong, nonatomic,nullable) ListSavedSearch *mdl;

@property (strong, nonatomic) NSString *accountToken;
@property (strong,nonatomic) NSMutableArray *reportContent;

- (IBAction)btnSaveReport:(id)sender;


@end
