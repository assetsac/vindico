//
//  Constants.m
//  Vindico
//
//  Created by Belinda Natividad on 8/23/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import "Constants.h"

@implementation Constants

#pragma mark - API URL KEYS
NSString *const kBaseUrl = @"http://www.vrdnet.net/api/public/api/v1/";

//// POST
//NSString *const kPostLoginAPIurlKey = @"auth/login";
//
//// GET
//NSString *const kGetAllUserChallenges = @"/challenges";

@end
