//
//  ListSavedReports.h
//  Vindico
//
//  Created by Belinda Natividad on 6/15/16.
//  Copyright © 2016 Belinda Natividad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ListSavedReports : NSObject

@property(nonatomic, strong) NSString           *reportId;
@property(nonatomic, strong) NSString           *reportName;
@property(nonatomic, strong) NSString           *reportCreatedDate;
@property(nonatomic, strong) NSMutableArray     *reportsArray;

- (instancetype)initWithData:(NSString*) reportItemId reportName:(NSString*) reportItemName createdDate:(NSString*) reportDate listReports:(NSArray*) reportListItems;

@end
